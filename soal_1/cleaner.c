#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>

//fungsi buat nyari SUSPICIOUS nya
int carisuspicious(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        return 0;
    }

    char line[1000];
    while (fgets(line, sizeof(line), file)) {
        if (strstr(line, "SUSPICIOUS")) {
            fclose(file);
            return 1;
        }
    }

    fclose(file);
    return 0;
}

//fungsi buat bikin cleaner.log
void buatlog(const char *path) {
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];
    strftime(timestamp, 20, "%Y-%m-%d %H:%M:%S", tm_info);

    FILE *log_file = fopen("/home/adfi/cleaner.log", "a");
    if (log_file) {
        fprintf(log_file, "[%s] '%s' has been removed.\n", timestamp, path);
        fclose(log_file);
    }
}

//fungsi buat ngecek foldernya udah bersih dari SUSPICIOUS apa belum
int folderbersih(const char *path) {
    struct dirent *entry;
    DIR *dir = opendir(path);
    if (!dir) {
        return 1;
    }

    while ((entry = readdir(dir))) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            closedir(dir);
            return 0; 
        }
    }

    closedir(dir);
    return 1; 
}

//fungsi buat ngehapus SUSPICIOUS
void hapussuspiscios(const char *path) {
    struct dirent *entry;
    struct stat statbuf;

    DIR *dir = opendir(path);
    if (!dir) {
        return;
    }

    chdir(path);  // Pindah ke direktori yang akan dicari SUSPICIOUS nya

    while ((entry = readdir(dir))) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        if (lstat(entry->d_name, &statbuf) == -1) {
            continue;
        }

        if (S_ISDIR(statbuf.st_mode)) {
            hapussuspiscios(entry->d_name);
        } else if (S_ISREG(statbuf.st_mode) && carisuspicious(entry->d_name)) {
            remove(entry->d_name);
            buatlog(entry->d_name);
        }
        sleep(30);
    }

    chdir("..");  // Kembali ke direktori sebelumnya
    closedir(dir);
}

int main(int argc, char *argv[]) {

    pid_t pid, sid;
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);
    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if (chdir("/") < 0) {
        exit(EXIT_FAILURE);
    }

    while (1) {
        hapussuspiscios(argv[1]);
        if (folderbersih(argv[1])) {
            break; // Keluar dari loop jika tidak ada SUSPICIOUS lagi
        }
    }

    return 0;
}

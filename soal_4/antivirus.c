#include <stdio.h>

#include <stdlib.h>

#include <string.h>

#include <unistd.h> // Mengimpor file header untuk sistem call POSIX (Unix-like).

#include <signal.h> // Mengimpor file header untuk menangani sinyal (signals).

#include <sys/types.h> // Mengimpor file header untuk tipe data khusus, seperti pid_t.

#include <sys/stat.h> // Mengimpor file header untuk mendapatkan informasi tentang file.

#include <dirent.h> // Mengimpor file header untuk berinteraksi dengan direktori.

#include <time.h> // Mengimpor file header untuk operasi waktu.

#define LOW 1 // Mendefinisikan konstanta LOW sebagai 1.
#define MEDIUM 2 // Mendefinisikan konstanta MEDIUM sebagai 2.
#define HARD 3 // Mendefinisikan konstanta HARD sebagai 3.

int securityLevel = LOW; // Variabel untuk menyimpan level keamanan, awalnya diatur ke LOW.

void logAction(char* filename, char* action); // Prototipe fungsi untuk mencatat tindakan.

void checkFile(char* filename) { // Fungsi untuk memeriksa file.
    char *ext = strrchr(filename, '.'); // Mendapatkan ekstensi file.
    if (ext != NULL) {
        ext++; // Mengabaikan titik

        FILE *extensionFile = fopen("extensions.csv", "r"); // Membuka file ekstensi.
        if (extensionFile != NULL) {
            char line[256]; // Variabel untuk menyimpan baris dari file ekstensi.
            while (fgets(line, sizeof(line), extensionFile)) { // Membaca setiap baris dari file ekstensi.
                line[strcspn(line, "\n")] = 0; // Menghilangkan newline character.
                if (strstr(line, ext) != NULL) { // Jika ekstensi cocok dengan yang ditemukan.
                    char action[20]; // Variabel untuk menyimpan tindakan yang akan dilakukan.
                    if (securityLevel == MEDIUM) {
                        sprintf(action, "Moved to quarantine"); // Menyimpan aksi sebagai "Moved to quarantine".
                        rename(filename, "quarantine"); // Memindahkan file ke karantina.
                    } else if (securityLevel == HARD) {
                        sprintf(action, "Removed"); // Menyimpan aksi sebagai "Removed".
                        remove(filename); // Menghapus file.
                    } else {
                        sprintf(action, "Logged"); // Menyimpan aksi sebagai "Logged".
                    }
                    logAction(filename, action); // Mencatat aksi ke dalam log.
                    break;
                }
            }
            fclose(extensionFile); // Menutup file ekstensi.
        } else {
            perror("Could not open extensions.csv"); // Menampilkan pesan kesalahan jika tidak dapat membuka file ekstensi.
            exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
        }
    }
}

void logAction(char* filename, char* action) { // Fungsi untuk mencatat tindakan.
    time_t t; // Variabel untuk menyimpan waktu.
    struct tm *tm_info; // Variabel untuk menyimpan informasi waktu terstruktur.

    time(&t); // Mendapatkan waktu saat ini.
    tm_info = localtime(&t); // Mengonversi waktu menjadi informasi waktu terstruktur.

    printf("[%s][%02d-%02d-%02d:%02d-%02d-%02d] - %s - %s\n", getenv("USER"), tm_info->tm_mday, tm_info->tm_mon + 1, tm_info->tm_year + 1900,
        tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, filename, action); //mencetak pesan log ke konsol dan menyimpannya dalam file virus.log

    FILE *logFile = fopen("virus.log", "a"); // Membuka file log.
    if (logFile != NULL) {
        fprintf(logFile, "[%s][%02d-%02d-%02d:%02d-%02d-%02d] - %s - %s\n", getenv("USER"), tm_info->tm_mday, tm_info->tm_mon + 1,
            tm_info->tm_year + 1900, tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, filename, action); // Menulis informasi ke dalam file log.
        fclose(logFile); // Menutup file log.
    }
}

void handleSignal(int signum) { // Fungsi untuk menangani sinyal.
    switch(signum) {
        case SIGUSR1:
            securityLevel = LOW; // Jika sinyal SIGUSR1 diterima, atur tingkat keamanan ke LOW.
            break;
        case SIGUSR2:
            securityLevel = MEDIUM; // Jika sinyal SIGUSR2 diterima, atur tingkat keamanan ke MEDIUM.
            break;
        case SIGUSR2 + 1:
            securityLevel = HARD; // Jika sinyal SIGRTMIN diterima, atur tingkat keamanan ke HARD.
            break;
        default:
            break;
    }
}

void startDaemon() { // Fungsi untuk memulai daemon.
    pid_t pid, sid; // Variabel untuk menyimpan PID dan SID.

    pid = fork(); // Melakukan fork untuk menciptakan proses baru.

    if (pid < 0) { // Jika fork gagal.
        exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
    }

    if (pid > 0) { // Jika ini adalah proses utama.
        exit(EXIT_SUCCESS); // Keluar dari program dengan sukses.
    }

    umask(0); // Mengatur izin untuk file yang baru dibuat.

    sid = setsid(); // Mendapatkan SID untuk sesi baru.

    if (sid < 0) { // Jika mendapatkan SID gagal.
        exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
    }

    if ((chdir("/")) < 0) { // Mengubah direktori kerja ke root.
        exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
    }

    close(STDIN_FILENO); // Menutup file descriptor standar input.
    close(STDOUT_FILENO); // Menutup file descriptor standar output.
    close(STDERR_FILENO); // Menutup file descriptor standar error.

    signal(SIGUSR1, handleSignal); // Mengatur penanganan sinyal untuk SIGUSR1.
    signal(SIGUSR2, handleSignal); // Mengatur penanganan sinyal untuk SIGUSR2.
    signal(SIGRTMIN, handleSignal); // Mengatur penanganan sinyal untuk SIGRTMIN.

    while (1) { // Loop utama daemon.
        DIR *dir; // Struktur untuk direktori.
        struct dirent *ent; // Struktur untuk entri direktori.

        if ((dir = opendir ("sisop_infected")) != NULL) { // Membuka direktori sisop_infected.
            while ((ent = readdir (dir)) != NULL) { // Membaca setiap entri di dalam direktori.
                if (ent->d_type != DT_REG) { // Memeriksa apakah entri adalah file non reguler.
                    checkFile(ent->d_name); // Memeriksa file.
                }
            }
            closedir (dir); // Menutup direktori.
        } else {
            perror ("Could not open directory"); // Menampilkan pesan kesalahan jika tidak dapat membuka direktori.
            exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
        }
        sleep(1); // Menunda eksekusi selama 1 detik.
    }
}

int main(int argc, char *argv[]) { // Fungsi utama.
    if (argc != 2) { // Memeriksa jumlah argumen.
        printf("Usage: %s <security_level>\n", argv[0]); // Menampilkan cara penggunaan program jika argumen tidak sesuai.
        exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
    }

    int level = atoi(argv[1]); // Mengonversi argumen ke integer.
    if (level == 1)
        securityLevel = LOW; // Jika level adalah 1, atur tingkat keamanan ke LOW.
    else if (level == 2)
        securityLevel = MEDIUM; // Jika level adalah 2, atur tingkat keamanan ke MEDIUM.
    else if (level == 3)
        securityLevel = HARD; // Jika level adalah 3, atur tingkat keamanan ke HARD.
    else {
        printf("Invalid security level\n"); // Jika level tidak valid, tampilkan pesan kesalahan.
        exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
    }

    startDaemon(); // Memulai daemon.

    return 0; // Keluar dari program dengan sukses.
}

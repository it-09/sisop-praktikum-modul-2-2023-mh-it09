# **LAPORAN RESMI PRAKTIKUM SISTEM OPERASI MODUL 2**
Berikut adalah Laporan Resmi Praktikum Sistem Operasi Modul 2 oleh Kelompok IT 09.

---
## **TABLE OF CONTENT**

 - [Soal 1](#soal1)
 - [Soal 2](#soal2)
 - [Soal 3](#soal3)
 - [Soal 4](#soal4)
 
---
### **I. SOAL 1<a name="soal1"></a>**
**A. PEMBAHASAN**
1. Pertama membuat fungsi untuk membuat cleaner.log, `buatlog` adalah fungsi yang digunakan untuk mencatat informasi tentang file yang dihapus ke dalam file .log. Fungsi ini menerima dua parameter: `path` dan `filename` yang merepresentasikan directory lengkap file yang dihapus. Fungsi ini menggunakan `time` untuk mendapatkan waktu saat ini dan `strftime` untuk mengonversi waktu menjadi format yang sesuai. Fungsi ini membuka file log /home/adfi/cleaner.log dalam mode tambahan ("a"), menulis informasi log, dan kemudian menutup file.

```c
void buatlog(const char *path, const char *filename) {
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];
    strftime(timestamp, 20, "%Y-%m-%d %H:%M:%S", tm_info);

    FILE *log_file = fopen("/home/adfi/cleaner.log", "a");
    if (log_file) {
        fprintf(log_file, "[%s] '%s/%s' has been removed.\n", timestamp, path, filename);
        fclose(log_file);
    }
}
```

2. Fungsi `carisuspicious` digunakan untuk mencari kata "SUSPICIOUS" dalam sebuah file. Fungsi ini menerima parameter `filename` yang adalah nama file yang akan diperiksa. Fungsi membuka file tersebut, membaca baris per baris, dan mencari kata "SUSPICIOUS" dalam setiap baris. Jika kata "SUSPICIOUS" ditemukan, maka fungsi akan mengembalikan 1; jika tidak ditemukan, maka akan mengembalikan 0.

```c
int carisuspicious(const char *filename) {
    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        return 0;
    }

    char line[1000];
    while (fgets(line, sizeof(line), file)) {
        if (strstr(line, "SUSPICIOUS")) {
            fclose(file);
            return 1;
        }
    }

    fclose(file);
    return 0;
}
```

3. Fungsi `folderbersih` ini digunakan untuk memeriksa apakah sebuah direktori bersih dari file yang mengandung kata "SUSPICIOUS" atau tidak. Fungsi ini menerima parameter `path`, yang adalah alamat direktori yang akan diperiksa. Fungsi membuka direktori tersebut dan memeriksa setiap file di dalamnya menggunakan fungsi `carisuspicious`. Jika ada file yang mengandung kata "SUSPICIOUS", variabel `hasSuspicious` akan diatur menjadi 1. Fungsi ini mengembalikan 1 jika direktori bersih (tidak ada file "SUSPICIOUS"), dan 0 jika tidak bersih.

```c
int folderbersih(const char *path) {
    struct dirent *entry;
    DIR *dir = opendir(path);
    if (!dir) {
        return 1;
    }

    int hasSuspicious = 0; // Tambahkan variabel untuk melacak adanya SUSPICIOUS dalam direktori

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            if (carisuspicious(entry->d_name)) {
                hasSuspicious = 1;
            }
        }
    }

    closedir(dir);

    return !hasSuspicious; // Mengembalikan 1 jika tidak ada SUSPICIOUS
}
```


4. FungSi `hapussuspicious` adalah fungsi utama yang digunakan untuk menghapus file yang mengandung kata "SUSPICIOUS" dalam sebuah direktori. Fungsi ini menerima parameter `path`, yang adalah alamat direktori yang akan diproses. Fungsi ini membuka direktori tersebut, mengakses setiap entri (file atau subdirektori), dan menghapus file jika mengandung kata "SUSPICIOUS". Fungsi juga melakukan rekursi ke dalam subdirektori untuk melakukan pemeriksaan dan penghapusan. Fungsi ini juga mencatat informasi penghapusan menggunakan fungsi `buatlog`. Terdapat juga pemanggilan `sleep(30)` untuk memberi jeda 30 detik setiap kali sebuah file "SUSPICIOUS" dihapus. Fungsi ini kemudian kembali ke direktori sebelumnya menggunakan `chdir("..")` dan menutup direktori yang sedang diproses.

```c
void hapussuspicious(const char *path) {
    struct dirent *entry;
    struct stat statbuf;

    DIR *dir = opendir(path);
    if (!dir) {
        return;
    }

    chdir(path);  // Pindah ke direktori yang akan dicari SUSPICIOUS nya

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
            continue;
        }

        if (lstat(entry->d_name, &statbuf) == -1) {
            continue;
        }

        if (S_ISDIR(statbuf.st_mode)) {
            hapussuspicious(entry->d_name);
        } else if (S_ISREG(statbuf.st_mode) && carisuspicious(entry->d_name)) {
            remove(entry->d_name);
            buatlog(path, entry->d_name);
        }
        sleep(30);
    }

    chdir("..");  // Kembali ke direktori sebelumnya
    closedir(dir);
}
```

5. Fungsi `main` adalah fungsi utama yang akan dijalankan saat program dimulai. Fungsi ini menggunakan `fork()` untuk menciptakan proses baru. Proses child akan menjadi daemon yang menjalankan operasi pembersihan. Proses parent/utama akan segera keluar setelah proses child dibuat. Fungsi ini menjalankan `hapussuspicious` dan `folderbersih` dalam loop hingga seluruh direktori bersih dari file "SUSPICIOUS". Program ini terus berjalan sebagai daemon sampai semua direktori bersih dari "SUSPICIOUS".

```c
int main(int argc, char *argv[]) {

    pid_t pid, sid;
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }
    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    umask(0);
    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    if (chdir("/") < 0) {
        exit(EXIT_FAILURE);
    }

    while (1) {
        hapussuspicious(argv[1]);
        if (folderbersih(argv[1])) {
            break; // Keluar dari loop jika tidak ada SUSPICIOUS lagi
        }
    }

    return 0;
}
```

6. Program diatur agar otomatis berhenti ketika tidak ada lagi file “SUSPICIOUS”

```c
while (1) {
        hapussuspicious(argv[1]);
        if (folderbersih(argv[1])) {
            break; // Keluar dari loop jika tidak ada SUSPICIOUS lagi
        }
    }
```


**B. HASIL**

- Susunan directory sebelum program jalan

![Screenshot_2023-10-12_065825](/uploads/485586cadf324793a8a969a8953c3410/Screenshot_2023-10-12_065825.png)

![Screenshot_2023-10-12_065727](/uploads/74ae9fcf09a62728ae8415bb36f19cc5/Screenshot_2023-10-12_065727.png)


- Susunan directory setelah program jalan

![Screenshot_2023-10-12_070719](/uploads/96a7bd05df5e7b547d87d7531a45a998/Screenshot_2023-10-12_070719.png)

![Screenshot_2023-10-12_070739](/uploads/093cfb307650f6a128151e8f795d03f0/Screenshot_2023-10-12_070739.png)

- cleaner.log

![Screenshot_2023-10-12_070550](/uploads/6048f84d2a22db5be77b6d02ca07aae1/Screenshot_2023-10-12_070550.png)


- Letak cleaner; cleaner.c; cleaner.log di /home/user

![Screenshot_2023-10-12_070653](/uploads/ae85d3703959954b89761d884a1e5a02/Screenshot_2023-10-12_070653.png)

**C. REVISI**

Menambahkan program untuk menampilkan alamat direktori lengkap di cleaner.log , karena sebelumnya cleaner.log hanya menampilkan nama file saja.

- Sebelum direvisi

![Screenshot_2023-10-12_073400](/uploads/5861deb252929c4d4148ffcd26eb1349/Screenshot_2023-10-12_073400.png)

```c
void buatlog(const char *path) {
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];
    strftime(timestamp, 20, "%Y-%m-%d %H:%M:%S", tm_info);

    FILE *log_file = fopen("/home/adfi/cleaner.log", "a");
    if (log_file) {
        fprintf(log_file, "[%s] '%s' has been removed.\n", timestamp, path);
        fclose(log_file);
    }
}
```

- Setelah direvisi

![Screenshot_2023-10-12_070550](/uploads/6048f84d2a22db5be77b6d02ca07aae1/Screenshot_2023-10-12_070550.png)
```c
void buatlog(const char *path, const char *filename) {
    time_t t = time(NULL);
    struct tm *tm_info = localtime(&t);
    char timestamp[20];
    strftime(timestamp, 20, "%Y-%m-%d %H:%M:%S", tm_info);

    FILE *log_file = fopen("/home/adfi/cleaner.log", "a");
    if (log_file) {
        fprintf(log_file, "[%s] '%s/%s' has been removed.\n", timestamp, path, filename);
        fclose(log_file);
    }
}
```

---

### **II. SOAL 2<a name="soal2"></a>**
**A. PEMBAHASAN**
1. Program pertama-tama membuat direktori "players" dengan menggunakan perintah `mkdir`. Itu dilakukan dengan menyimpan perintah dan argumennya dalam array `buatPlayers`, dan kemudian fork proses untuk menjalankan perintah tersebut.

```c
    char *buatPlayers[] = {"mkdir", "players", NULL};
    pid_t pid = fork();
```

2. Selanjutnya, kode berikut berada di dalam cabang `if (pid == 0)`, yang hanya akan dijalankan oleh proses child. Proses child menggunakan `execv` untuk menjalankan perintah `mkdir` dengan argumen yang telah disiapkan dalam array buatPlayers. Setelah itu, proses child keluar. Sementara itu, dalam cabang `else`, proses parents menunggu proses child selesai dengan menggunakan `waitpid`. Ini memungkinkan program untuk melanjutkan hanya setelah proses child selesai.

```c
    if (pid == 0) {
        // Proses Child untuk membuat folder "players"
        execv("/bin/mkdir", buatPlayers);
        exit(1);
    } else {
        int status;
        waitpid(pid, &status, 0);
```

3. Kemudian program ini menggunakan `wget` untuk mengunduh file database pemain dari URL yang diberikan. Ini dilakukan dengan menyimpan perintah dan argumennya dalam array `donlodFile`, kemudian melakukan fork proses untuk menjalankan perintah tersebut.

```c
        // Download file database pemain
        char *donlodFile[] = {"wget", "https://drive.google.com/uc?id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "-O", "players.zip", NULL};
        pid = fork();
```


4. Kode ini mirip dengan yang sebelumnya, di mana proses child menggunakan `execv` untuk menjalankan perintah `wget` dan kemudian keluar, sedangkan proses parents menunggu proses child nya selesai.
```c
        if (pid == 0) {
            execv("/usr/bin/wget", donlodFile);
            exit(1);
        } else {
            waitpid(pid, &status, 0);

```

5. Setelah mengunduh file, program ini menggunakan `unzip` untuk mengekstrak isinya ke dalam folder "players". Persiapan perintah dan fork proses dilakukan seperti sebelumnya.
```c
        // Ekstrak "players.zip" ke dalam folder "players"
        char *unzipPlayers[] = {"unzip", "players.zip", "-d", "players", NULL};
        pid = fork();

```

6. Kode ini mirip dengan sebelumnya, di mana proses child menjalankan `unzip` dan proses parents menunggu hingga selesai.
```c
        if (pid == 0) {
            execv("/usr/bin/unzip", unzipPlayers);
            exit(1);
        } else {
            waitpid(pid, &status, 0);
```

7. Setelah mengekstrak file, program ini menghapus file "players.zip" yang tidak lagi diperlukan.
```c
            // Hapus file "players.zip"
        char *hapusPlayerszip[] = {"rm", "players.zip", NULL};
        pid = fork();

```

8. Kode ini mirip dengan yang sebelumnya, di mana proses child menjalankan perintah `rm` dan proses parents menunggu hingga selesai.
```c
        if (pid == 0) {
            execv("/bin/rm", hapusPlayerszip);
            exit(1);
        } else {
            waitpid(pid, &status, 0);

```

9. Setelah proses-proses sebelumnya selesai, program ini mulai mengelola direktori pemain. Pertama-tama, ia membuka direktori "players" dengan `opendir`. Kemudian, program ini menggunakan loop `while` untuk menelusuri file dalam direktori "players" dan memprosesnya. Ini adalah bagian yang akan memeriksa nama file dan menghapus file-foto pemain yang bukan dari tim "Cavaliers". Dalam loop, program ini memeriksa nama file yang dibaca dari direktori. File-foto pemain yang bukan dari tim "Cavaliers" akan dihapus dengan menggunakan perintah `remove`.
```c
DIR *nonCavaliers = opendir("players");
                    struct dirent *entry;

                    if (nonCavaliers != NULL) {
                        while ((entry = readdir(nonCavaliers)) != NULL) {
                            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                                char *namaFoto = entry->d_name;

                                if (strstr(namaFoto, "Cavaliers") == NULL) {
                                    char path[256];
                                    snprintf(path, sizeof(path), "players/%s", namaFoto);
                                    remove(path);
                                }
                            }
                        }
                        closedir(nonCavaliers);
                    }

```


10. Selanjutnya, program ini membuka direktori "players" kembali, dan kali ini akan mengkategorikan pemain berdasarkan posisinya. Variabel-variabel seperti `pgHitung`, `sgHitung`, `sfHitung`, `pfHitung `, ` cHitung` , akan digunakan untuk menghitung jumlah pemain dalam setiap posisi.
```c
       // Kategori pemain dan hitung jumlah pemain untuk masing-masing posisi
                    int pgHitung = 0, sgHitung = 0, sfHitung = 0, pfHitung = 0, cHitung = 0;
                    DIR *mengKategori = opendir("players");
                    if (mengKategori != NULL) {
                        while ((entry = readdir(mengKategori)) != NULL) {
                            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                                char *namaFoto = entry->d_name;
                                char *pos = strtok(namaFoto, "-");

                                if (pos != NULL) {
                                    if (strcmp(pos, "Cavaliers") == 0) {
                                        pos = strtok(NULL, "-");

                                        if (pos != NULL) {
                                            if (strcmp(pos, "PG") == 0) {
                                                pgHitung++;
                                            } else if (strcmp(pos, "SG") == 0) {
                                                sgHitung++;
                                            } else if (strcmp(pos, "SF") == 0) {
                                                sfHitung++;
                                            } else if (strcmp(pos, "PF") == 0) {
                                                pfHitung++;
                                            } else if (strcmp(pos, "C") == 0) {
                                                cHitung++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        closedir(mengKategori);
                    }
```
11. Selanjutnya program ini menggunakan loop yang mirip dengan sebelumnya untuk menelusuri file-foto pemain dalam direktori "players". Ini akan memproses nama file-foto pemain, memisahkan posisi pemain, dan menghitung jumlah pemain dalam masing-masing posisi. Kemudian, program ini membuka file "Formasi.txt" untuk menulis hasil kategorisasi. Program ini menggunakan `fprintf` untuk menulis hasil kategorisasi ke dalam file "Formasi.txt". Setelah selesai, file ditutup. 
```c
 // Output hasil kategorisasi ke "Formasi.txt"
                    FILE *outputFile = fopen("Formasi.txt", "w");

                    if (outputFile != NULL) {
                        fprintf(outputFile, "PG: %d\nSG: %d\nSF: %d\nPF: %d\nC: %d\n", pgHitung, sgHitung, sfHitung, pfHitung, cHitung);
                        fclose(outputFile);
                    }
```

12. Setelah mengkategorikan pemain, program ini membuat direktori "clutch" dengan menggunakan perintah `mkdir`. Setelah membuat direktori "clutch", program ini menyalin file foto pemain Kyrie Irving ke dalam folder "clutch" dengan menggunakan perintah `cp`. Kode ini adalah proses yang mirip dengan yang sebelumnya juga, di mana proses child menjalankan perintah `cp`, dan proses parents menunggu hingga selesai. Setelah menyalin foto Kyrie Irving, program ini juga menyalin foto pemain LeBron James ke dalam folder "clutch". Kode ini adalah proses yang mirip dengan yang sebelumnya, di mana proses anak menjalankan perintah cp, dan proses orangtua menunggu hingga selesai.

```c
// Membuat folder "clutch"
                    char *bikinClutch[] = {"mkdir", "clutch", NULL};
                    pid = fork();

                    if (pid == 0) {
                        execv("/bin/mkdir", bikinClutch);
                        
                        exit(1);
                    } else {
                        waitpid(pid, &status, 0);

                        // Copy foto Kyrie Irving ke dalam folder "clutch"
                        char *copyKyrie[] = {"cp", "players/Cavaliers-PG-Kyrie-Irving.png", "clutch/Kyrie.png", NULL};
                        pid = fork();

                        if (pid == 0) {
                            execv("/bin/cp", copyKyrie);
                            exit(1);
                        } else {
                            waitpid(pid, &status, 0);

                            // Copy foto Lebron James ke dalam folder "clutch"
                            char *copyLebron[] = {"cp", "players/Cavaliers-SF-LeBron-James.png", "clutch/Lebron.png", NULL};
                            pid = fork();

                            if (pid == 0) {
                                execv("/bin/cp", copyLebron);
                                exit(1);
                            } else {
                                waitpid(pid, &status, 0);
                            }
                        }
                    }
```

**B. HASIL**

- Folder Clutch

![Screenshot_2023-10-13_115919](/uploads/9c76550be8916ea7cf02bd38456b89cc/Screenshot_2023-10-13_115919.png)


- Folder Players yang berisi pemain Cavaliers

![Screenshot_2023-10-13_115907](/uploads/aa9ee118b1f4b302f5e69f081b749de0/Screenshot_2023-10-13_115907.png)

- File Formasi.txt

![Screenshot_2023-10-13_120002](/uploads/513e7d87918d0b884d4c103145a7da25/Screenshot_2023-10-13_120002.png)

- Susunan Directory Keseluruhan

![Screenshot_2023-10-13_120026](/uploads/5f02aa84a3ca11b873e17a0fc36e01cf/Screenshot_2023-10-13_120026.png)

**C. REVISI**

Tidak ada.

---

### **III. SOAL 3<a name="soal3"></a>**
**A. PEMBAHASAN**
1. Pada poin A, akan digunakan fungsi `create_folder` untuk membuat folder setiap 30 detik dengan format nama timestamp.
```c
void create_folder(){
	pid_t pid = fork();
	if (pid < 0){
    	exit(EXIT_FAILURE);
	}
	else if (pid == 0){
    	char foldername[120];
    	time_t t = time(NULL);
    	struct tm *current = localtime(&t);
    	sprintf(foldername, "/home/caca/Downloads/soal_3/%04d-%02d-%02d_%02d:%02d:%02d", current->tm_year + 1900,
            	current->tm_mon + 1, current->tm_mday, current->tm_hour, current->tm_min, current->tm_sec);  
    	char *args[] = {"mkdir", foldername, NULL};
    	execv("/bin/mkdir", args);
	}
}
```
Fungsi ini menggunakan pid_t pid = fork() untuk menciptakan child process yang disimpan ke variabel pid. Jika `fork()` mengembalikan nilai negatif, berarti terjadi kesalahan dalam menciptakan child process. Dalam hal ini, program keluar dengan kode kesalahan menggunakan `exit(EXIT_FAILURE)`. Jika fork nya berhasil dimana pid akan bernilai 0, maka dideklarasikan variabel foldername. Program membuat nama folder dengan format tertentu menggunakan informasi waktu saat ini, mencakup tahun, bulan, hari, jam, menit, dan detik. Ini dilakukan dengan menggunakan fungsi `time(NULL)` untuk mendapatkan waktu saat ini dan `localtime(&t)` untuk mengonversi waktu dalam bentuk struktur `tm` lokal. Kemudian, nama folder disusun menggunakan `sprintf()`. Setelah nama folder dibuat, program mempersiapkan argumen untuk menjalankan perintah `mkdir` menggunakan `execv()`. Variabel `char *args[]` berisi argumen yang akan digunakan saat menjalankan perintah `mkdir`. Fungsi `execv()` digunakan untuk menjalankan perintah tersebut dalam child process. Setelah `execv()` dijalankan, child process akan berhenti dan program akan keluar.
Selanjutnya, fungsi `create_folder()` dipanggil di fungsi main agar dijalankan menggunakan while loop setiap 30 detik.
```c
    	while (1) {
        	create_folder();
        	sleep(30);
    	}
```

2. Pada poin B, Tiap-tiap folder diisi dengan 15 gambar yang di download dari https://source.unsplash.com/{widthxheight} , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran **(t%1000)+50** dengan format nama timestamp **[YYYY-mm-dd_HH:mm:ss].**
```c
void images_load(char folderpath[]){
	pid_t id_child = fork();
	if (id_child < 0){
    	exit(EXIT_FAILURE);
	}
	else if (id_child == 0){
    	char filename[500], images_url[120];
    	time_t t = time(NULL);
    	struct tm *current = localtime(&t);
    	int imageSize = (int)(t % 1000) + 50;


    	sprintf(filename, "%s/%04d-%02d-%02d_%02d:%02d:%02d", folderpath, current->tm_year + 1900,
            	current->tm_mon + 1, current->tm_mday, current->tm_hour, current->tm_min, current->tm_sec);  
    	sprintf(images_url, "https://source.unsplash.com/random/%dx%d", imageSize, imageSize);


    	char *args[] = {"wget", "-q", "-O", filename, images_url, NULL};
    	execv("/bin/wget", args);
	}
}
```
Fungsi ini menerima parameter folderpath yang merupakan path ke directory tempat gambar disimpan, lalu menggunakan fork untuk menciptakan child process yang baru. Kemudian menggunakan if else untuk mengidentifikasi fork, jika forknya sukses (bernilai 0), maka akan dideklarasikan variabel filename dan images url. Deklarasi time_t t = time digunakan untuk mendapatkan waktu saat ini dalam bentuk timestamp yang menggunakan variabel current yang menunjuk ke tm untuk menyimpan komponen waktu, dan mengatur image sizenya sesuai dengan ukuran yang ditentukan soal.
Nama file dibuat dengan format yang mencakup tahun, bulan, hari, jam, menit, dan detik dari waktu saat ini, serta folderpath yang telah diberikan. Ini dilakukan dengan menggunakan `sprintf()`. Selain itu, program juga membuat URL gambar yang akan diunduh dari sumber eksternal dengan format tertentu. Program kemudian mempersiapkan argumen untuk menjalankan perintah `wget` menggunakan `execv()`. Variabel `char *args[]` berisi argumen yang akan digunakan saat menjalankan perintah `wget`. Fungsi `execv()` digunakan untuk menjalankan perintah `wget` dalam child process. Ini menggantikan child process saat ini dengan proses `wget`. Setelah `execv()` dijalankan, child process akan berhenti dan program akan keluar.
Fungsi `images_load()` kemudian dipanggil dalam fungsi main agar pengunduhan gambar dijalankan setiap 5 detik selama jumlah file di dalam 1 folder masih <15.
```c
if (S_ISDIR(sb.st_mode)) {
                    	int count = files_counting(folderpath);
                    	if (count < 15) images_load(folderpath);
                    	else create_zip(path, entry->d_name);
                	}
            	}
            	closedir(dirPtr);
        	}
        	sleep(5);
```
Jumlah file dihitung menggunakan fungsi `files_counting`. 
```c
int files_counting(char folderpath[]){
	DIR *dirPtr;
	struct dirent *entry;
	int count = 0;
	dirPtr = opendir(folderpath);


	if (dirPtr != NULL) {
    	while ((entry = readdir (dirPtr))) {
        	if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;
        	else count++;
    	}
    	closedir (dirPtr);
	}


	return count;
}
```
Fungsi ini membuka direktori yang ditentukan oleh `folderpath` menggunakan fungsi `opendir()`. Jika direktori berhasil dibuka, maka `dirPtr` akan menunjuk ke direktori tersebut. Kemudian variabel count diinisialisasi dengan nilai 0. Fungsi menggunakan loop `while` untuk mengiterasi melalui isi direktori yang sudah dibuka. Setiap entri dalam direktori akan dibaca menggunakan fungsi `readdir()`. Jika nama entri tidak sama dengan "." atau "..", maka `count` akan bertambah, menunjukkan bahwa ada satu file atau direktori lain yang ada dalam direktori yang sedang dihitung. Setelah selesai mengiterasi melalui seluruh isi direktori, direktori ditutup menggunakan `closedir()` dan akan mengembalikan nilai `count` yang merupakan jumlah file dan direktori dalam direktori yang ditentukan oleh `folderpath`.

3. Pada poin C, folder akan dikompresi menjadi zip, dengan format nama **[YYYY-mm-dd_HH:mm:ss].zip.**
```c
void create_zip(char path[], char zipName[]){
pid_t id_child = fork();
	if (id_child < 0) {
    	perror("Fork failed");
    	exit(EXIT_FAILURE);
	} else if (id_child == 0) {
    	chdir(path);
    	char *args[] = {"zip", "-r", "-m", zipName, zipName, NULL};
    	execvp("zip", args);
    	perror("Exec failed");
    	exit(EXIT_FAILURE);
	}
}
```
Fungsi ini menerima 2 parameter, yaitu path (lokasi directory yang akan dizip) dan zipname (nama untuk file zip yang akan dibuat). Lalu akan dilakukan fork untuk menghasilkan child process yang hasilnya disimpan dalam variabel id child, jika failed maka akan keluar program dan menampilkan pesan error, sedangkan jika sukses, dimana id childnya bernilai 0, Fungsi akan mengubah direktori kerja aktif ke direktori yang ditentukan oleh `path` menggunakan `chdir()`. Lalu menyiapkan argumen untuk menjalankan perintah `zip`. Argumen ini mencakup perintah `-r` (rekursif) untuk mengompres seluruh isi direktori, `-m` untuk menghapus file asli setelah mengompres, dan `zipName` sebagai nama arsip ZIP yang akan dibuat. Dan menjalankan perintah `zip` dalam menggunakan `execvp()`.
Fungsi ini dipanggil di fungsi main pada block if else, dimana jika variabel count tidak bernilai <15, maka `create_zip()` akan dipanggil dan menghasilkan zip folder.
```c
if (S_ISDIR(sb.st_mode)) {
                    	int count = files_counting(folderpath);
                    	if (count < 15) images_load(folderpath);
                    	else create_zip(path, entry->d_name);
                	}
```

4. Pada poin D&E, akan dibuat killer yang dapat dijalankan dalam 2 mode, yaitu a dan b. 
```c
void create_killer(pid_t pid, char p){
	char current_dir[100];
	getcwd(current_dir, sizeof(current_dir));
	int status;
	pid_t parent_process = fork();
	if (parent_process < 0)
    	exit(EXIT_FAILURE);


	if (parent_process == 0) {
    	FILE *filePtr;
    	char killer_file[150];
    	char code[1000];
    	char exec_path[150];
    	char file_name[150];
    	char run_mode[100];


    	sprintf(file_name, "%s/killer.c", current_dir);
    	sprintf(exec_path, "%s/killer", current_dir);


    	sprintf(killer_file, "%s/killer.c", current_dir);
```
Kode pertama mengambil direktori kerja saat ini menggunakan `getcwd` dan menyimpannya dalam variabel `current_dir`. Jika proses fork berhasil, dimana bernilai 0, maka program akan membuat file "killer.c" dalam direktori saat ini dengan kode yang akan dijalankan nantinya. Isi kode yang akan dibuat tergantung pada mode yang diberikan `(p == ‘a’ atau p == ‘b’)`. 
```c
    	if (p == 'a')
      	strcpy(run_mode, " char *argv[] = {\"killall\", \"-s\", \"9\", \"lukisan\", NULL}; execv(\"/usr/bin/killall\", argv);");
    	else if (p == 'b')
      	sprintf(run_mode, " char *argv[] = {\"kill\", \"%d\", NULL}; execv(\"/usr/bin/kill\", argv);", pid);


    	sprintf(code,   "#include <stdlib.h>\n"
                    	"#include <unistd.h>\n"
                    	"#include <stdio.h>\n"
                    	"#include <wait.h>\n"
                    	"#include <string.h>\n"
                    	"#include <sys/types.h>\n"


                    	" int main() {"
                    	" pid_t id_child;"
                    	" int status;"


                    	" id_child = fork();"
                    	" if (id_child < 0) exit(EXIT_FAILURE);"


                    	" if (id_child == 0) {"
                        	" %s"
                    	" } else {"
                        	" while (wait(&status)>0);"
                        	" char *argv[] = {\"rm\", \"%s/killer\", NULL};"
                        	" execv(\"/bin/rm\", argv);"
                    	" }"
                    	" }", run_mode, current_dir);


    	filePtr = fopen(killer_file, "w");
    	fputs(code, filePtr);
    	fclose(filePtr);


    	char *argv[] = {"gcc", file_name, "-o", exec_path, NULL};
    	execv("/usr/bin/gcc", argv);
	}
	while(wait(&status) > 0);
```
Mode 'a' akan menghentikan dan menghapus semua proses dengan nama "lukisan" menggunakan perintah `killall`. Mode 'b' akan menghentikan proses dengan ID `pid` yang telah diberikan menggunakan perintah `kill`. Setelah membuat file "killer.c", program akan mengkompilasi file tersebut menjadi sebuah executable bernama "killer" menggunakan perintah `gcc`.
```c
pid_t remove_killer = fork();


	if (remove_killer < 0)
  	exit(EXIT_FAILURE);


	if (remove_killer == 0) {
    	char killer_file_path[150];
    	sprintf(killer_file_path, "%s/killer.c", current_dir);
    	char *argv[] = {"rm", killer_file_path, NULL};
    	execv("/bin/rm", argv);
	}


	while(wait(&status) > 0);
}
```
Setelah kompilasi selesai, program akan menciptakan child process kedua untuk menghapus file "killer.c" dari direktori saat ini. Program akan mempersiapkan argumen untuk menjalankan perintah `rm` (remove) untuk menghapus file "killer". Ini dilakukan dengan menginisialisasi variabel `killer_file_path` yang berisi path lengkap ke file "killer". Setelah itu, program menggunakan `execv()` untuk menjalankan perintah `rm` dengan argumen yang sesuai. Proses induk akan menunggu hingga child process kedua selesai menggunakan `wait(&status)`. Dengan ini, program killer akan menghapus dirinya sendiri setelah dijalankan.
Fungsi `create_killer()` kemudian dipanggil pada fungsi main dalam child process pertama.
```c
int main(int argc, char *argv[]) {
	if(argc != 2 || argv[1][1] != 'a' && argv[1][1] != 'b'){
    	printf("Argumen invalid \n");
    	return 0;
	}


	pid_t pid1, pid_2;


	pid1 = fork();
	if (pid1 == 0) {


    	create_killer(getpid(), argv[1][1]);
    	umask(0);


    	pid_t sid = setsid();
    	if (sid < 0) {
        	exit(EXIT_FAILURE);
    	}


    	if ((chdir("/")) < 0) {
        	exit(EXIT_FAILURE);
    	}


    	close(STDIN_FILENO);
    	close(STDOUT_FILENO);
    	close(STDERR_FILENO);


    	while (1) {
        	create_folder();
        	sleep(30);
    	}
	} else if (pid1 < 0) {
    	exit(EXIT_FAILURE);
	}
```
Program ini memeriksa apakah jumlah argumen adalah 2 (argumen mode 'a' atau 'b') dan apakah argumen kedua adalah 'a' atau 'b'. Setelah pemeriksaan argumen, program menciptakan child process pertama (pid1) menggunakan `fork()`. Child process pertama ini akan digunakan untuk membuat direktori dan mengunduh gambar. Child process pertama melakukan beberapa pengaturan daemon dengan mengatur `umask` ke 0 untuk memberikan izin penuh pada file yang akan dibuat, `setsid()` untuk membuat session ID baru agar bisa berjalan secara independen dari terminal, kemudian mengganti direktori kerja aktif ke direktori root ("/") agar tidak bergantung pada direktori kerja awal dan menutup file descriptor standar (stdin, stdout, stderr) untuk memisahkan diri dari terminal. Kemudian, child process pertama masuk ke dalam loop utama. Dalam loop ini, program akan menjalankan fungsi `create_folder()` yang bertugas membuat folder baru dan kemudian menjalankan `images_load()` untuk mengunduh gambar ke folder tersebut setiap 30 detik.
```c
pid_2 = fork();
	if (pid_2 == 0) {


    	umask(0);


    	pid_t sid = setsid();
    	if (sid < 0) {
        	exit(EXIT_FAILURE);
    	}


    	if ((chdir("/")) < 0) {
        	exit(EXIT_FAILURE);
    	}


    	close(STDIN_FILENO);
    	close(STDOUT_FILENO);
    	close(STDERR_FILENO);


    	while (1) {
        	DIR *dirPtr;
        	struct dirent *entry;
        	char *path = "/home/caca/Downloads/soal_3";
        	dirPtr = opendir(path);
        	struct stat sb;


        	if (dirPtr != NULL) {
            	while ((entry = readdir(dirPtr))) {
                	if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;


                	char folderpath[350];
                	sprintf(folderpath, "%s/%s", path, entry->d_name);
                	if (stat(folderpath, &sb) == -1) continue;


                	if (S_ISDIR(sb.st_mode)) {
                    	int count = files_counting(folderpath);
                    	if (count < 15) images_load(folderpath);
                    	else create_zip(path, entry->d_name);
                	}
            	}
            	closedir(dirPtr);
        	}
        	sleep(5);
    	}
	} else if (pid_2 < 0) {
    	exit(EXIT_FAILURE);
	}


	return 0;
}
```
Setelah ciptaan child process pertama, program menciptakan child process kedua (pid_2) untuk tugas lain menggunakan `fork()`. Sama dengan sebelumnya, proses ini akan melakukan beberapa pengaturan daemon. Proses ini juga masuk ke dalam loop utama, dan membuka direktori yang akan dipantau, yaitu "/home/caca/Downloads/soal_3", menggunakan fungsi `opendir()`. Dalam loop, proses akan mengiterasi melalui isi direktori menggunakan fungsi `readdir()`. Kemudian memeriksa apakah entri adalah "." (direktori saat ini) atau ".." (direktori induk). Jika entri adalah salah satu dari keduanya, maka program akan melanjutkan ke entri selanjutnya tanpa tindakan apapun. Untuk setiap sub-direktori yang valid, child process kedua menggunakan `stat()` untuk memeriksa jenis entri tersebut. Jika entri adalah direktori, maka program akan memanggil fungsi `files_counting()`, `images_load()`, dan `create_zip()` seperti yang sudah dijelaskan pada poin A-C. Loop ini akan berjalan terus menerus, memeriksa dan mengelola sub-direktori sesuai dengan jumlah file yang ada di dalamnya.  

**B. HASIL**  
Screenshot program selama dijalankan:
1. Compile file “lukisan.c” menjadi executable “lukisan”.

![Picture1](/uploads/39d7d9fd009dacbffcca1932fb708d44/Picture1.png)


2. Tampilan terminal

![Picture2](/uploads/09dd8d8ca41598f2c24481b28a4ee9a2/Picture2.png)


3. Menjalankan lukisan dalam mode -a menggunakan command `./lukisan -a`.

![Picture3](/uploads/6a573d031a3d4bcd5dac0606ae11eb39/Picture3.png)
<br>
isi dari folder ketika proses download gambar:
<br>
![Picture4](/uploads/35d99e169c1272c0a13d26020e073419/Picture4.png)


4. Menjalankan killer dalam mode -a menggunakan command `./killer`

![Picture5](/uploads/bbb14d91a42f6540dcc909a74bf8c624/Picture5.png)
<br>
Killer dalam mode a akan membuat seluruh proses berhenti saat itu juga dan killer akan menghapus dirinya sendiri.
<br>
![Picture6](/uploads/88ec0375c9eae36b2979fa9b0d3c77c0/Picture6.png)


5. Menjalankan lukisan dalam mode -b menggunakan command `./lukisan -b`

![Picture7](/uploads/c527a093fa1125e9c9249b761cdbe8c9/Picture7.png)


6. Menjalankan killer dalam mode -b menggunakan command `./killer`

![Picture8](/uploads/7ebfc7104d813abad760027d90750106/Picture8.png)
<br>
Killer dalam mode b baru akan menghentikan proses ketika seluruh proses telah dijalankan. Jadi, proses pembuatan folder tiap 30 detik akan langsung berhenti saat killer dijalankan, namun tiap folder yang sudah terbentuk akan meneruskan prosesnya untuk mendownload 15 gambar dan mengompresi dirinya menjadi zip. Killer akan menghapus dirinya sendiri.


7. Isi dari folder zip

![Picture9](/uploads/dd70f183e58648039ed6b20971d502ad/Picture9.png)


**C. REVISI**  
Tidak ada.  

---

### **IV. SOAL 4<a name="soal4"></a>**

**A. PEMBAHASAN**
1. Secara keseluruhan soal nomor 4 mengharuskan kita untuk membuat program `antivirus.c` yang dimana program tersebut dapat memeriksa file di folder sisop_infected, dan jika file tersebut diidentifikasi sebagai virus berdasarkan ekstensinya; program harus memindahkannya ke folder quarantine; berjalan di latar belakang tanpa adanya intervensi; setiap kali pendeteksian virus harus dicatat dalam virus.log; terdapat 3 jenis level keamanan yaitu low, medium, hard; dapat mengganti level keamanan dari antivirus tanpa harus menghentikannya; dan terdapat fitur untuk mematikan antivirus dengan cara yang aman dan efisien.

2. Sebelum kita membuat program `antivirus.c` , alangkah baiknya kita mendownload file `extension.csv`. 
```c
wget --no-check-certificate 'https://drive.google.com/uc?export=download&id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5' -O extensions.csv
```
Dalam 8 baris pertama file `extension.csv` itu tidak dienkripsi. Sehingga baris-baris setelahnya perlu dilakukan dekripsi menggunakan algoritma rot13 untuk mengetahui ekstensi virus lainnya.

3. Setelah itu kita bisa membuat `folder sisop_infected` dan `folder quarantine`.

4. Lalu di dalam `folder sisop_infected` dapat dibuat dummy virus test dengan extension yang sesuai dengan `extensions.csv`. Misalnya adalah seperti berikut:
![foto_1](/uploads/a715bc01b89273ad1f4ded22ea605c25/foto_1.PNG)

5. Setelah itu kita bisa langsung membuat `antivirus.c`. Berikut ini adalah program yang telah saya buat.
```c
#include <stdio.h>

#include <stdlib.h>

#include <string.h>

#include <unistd.h> // Mengimpor file header untuk sistem call POSIX (Unix-like).

#include <signal.h> // Mengimpor file header untuk menangani sinyal (signals).

#include <sys/types.h> // Mengimpor file header untuk tipe data khusus, seperti pid_t.

#include <sys/stat.h> // Mengimpor file header untuk mendapatkan informasi tentang file.

#include <dirent.h> // Mengimpor file header untuk berinteraksi dengan direktori.

#include <time.h> // Mengimpor file header untuk operasi waktu.

#define LOW 1 // Mendefinisikan konstanta LOW sebagai 1.
#define MEDIUM 2 // Mendefinisikan konstanta MEDIUM sebagai 2.
#define HARD 3 // Mendefinisikan konstanta HARD sebagai 3.

int securityLevel = LOW; // Variabel untuk menyimpan level keamanan, awalnya diatur ke LOW.

void logAction(char* filename, char* action); // Prototipe fungsi untuk mencatat tindakan.

void checkFile(char* filename) { // Fungsi untuk memeriksa file.
    char *ext = strrchr(filename, '.'); // Mendapatkan ekstensi file.
    if (ext != NULL) {
        ext++; // Mengabaikan titik

        FILE *extensionFile = fopen("extensions.csv", "r"); // Membuka file ekstensi.
        if (extensionFile != NULL) {
            char line[256]; // Variabel untuk menyimpan baris dari file ekstensi.
            while (fgets(line, sizeof(line), extensionFile)) { // Membaca setiap baris dari file ekstensi.
                line[strcspn(line, "\n")] = 0; // Menghilangkan newline character.
                if (strstr(line, ext) != NULL) { // Jika ekstensi cocok dengan yang ditemukan.
                    char action[20]; // Variabel untuk menyimpan tindakan yang akan dilakukan.
                    if (securityLevel == MEDIUM) {
                        sprintf(action, "Moved to quarantine"); // Menyimpan aksi sebagai "Moved to quarantine".
                        rename(filename, "quarantine"); // Memindahkan file ke karantina.
                    } else if (securityLevel == HARD) {
                        sprintf(action, "Removed"); // Menyimpan aksi sebagai "Removed".
                        remove(filename); // Menghapus file.
                    } else {
                        sprintf(action, "Logged"); // Menyimpan aksi sebagai "Logged".
                    }
                    logAction(filename, action); // Mencatat aksi ke dalam log.
                    break;
                }
            }
            fclose(extensionFile); // Menutup file ekstensi.
        } else {
            perror("Could not open extensions.csv"); // Menampilkan pesan kesalahan jika tidak dapat membuka file ekstensi.
            exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
        }
    }
}

void logAction(char* filename, char* action) { // Fungsi untuk mencatat tindakan.
    time_t t; // Variabel untuk menyimpan waktu.
    struct tm *tm_info; // Variabel untuk menyimpan informasi waktu terstruktur.

    time(&t); // Mendapatkan waktu saat ini.
    tm_info = localtime(&t); // Mengonversi waktu menjadi informasi waktu terstruktur.

    printf("[%s][%02d-%02d-%02d:%02d-%02d-%02d] - %s - %s\n", getenv("USER"), tm_info->tm_mday, tm_info->tm_mon + 1, tm_info->tm_year + 1900,
        tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, filename, action); //mencetak pesan log ke konsol dan menyimpannya dalam file virus.log

    FILE *logFile = fopen("virus.log", "a"); // Membuka file log.
    if (logFile != NULL) {
        fprintf(logFile, "[%s][%02d-%02d-%02d:%02d-%02d-%02d] - %s - %s\n", getenv("USER"), tm_info->tm_mday, tm_info->tm_mon + 1,
            tm_info->tm_year + 1900, tm_info->tm_hour, tm_info->tm_min, tm_info->tm_sec, filename, action); // Menulis informasi ke dalam file log.
        fclose(logFile); // Menutup file log.
    }
}

void handleSignal(int signum) { // Fungsi untuk menangani sinyal.
    switch(signum) {
        case SIGUSR1:
            securityLevel = LOW; // Jika sinyal SIGUSR1 diterima, atur tingkat keamanan ke LOW.
            break;
        case SIGUSR2:
            securityLevel = MEDIUM; // Jika sinyal SIGUSR2 diterima, atur tingkat keamanan ke MEDIUM.
            break;
        case SIGUSR2 + 1:
            securityLevel = HARD; // Jika sinyal SIGRTMIN diterima, atur tingkat keamanan ke HARD.
            break;
        default:
            break;
    }
}

void startDaemon() { // Fungsi untuk memulai daemon.
    pid_t pid, sid; // Variabel untuk menyimpan PID dan SID.

    pid = fork(); // Melakukan fork untuk menciptakan proses baru.

    if (pid < 0) { // Jika fork gagal.
        exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
    }

    if (pid > 0) { // Jika ini adalah proses utama.
        exit(EXIT_SUCCESS); // Keluar dari program dengan sukses.
    }

    umask(0); // Mengatur izin untuk file yang baru dibuat.

    sid = setsid(); // Mendapatkan SID untuk sesi baru.

    if (sid < 0) { // Jika mendapatkan SID gagal.
        exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
    }

    if ((chdir("/")) < 0) { // Mengubah direktori kerja ke root.
        exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
    }

    close(STDIN_FILENO); // Menutup file descriptor standar input.
    close(STDOUT_FILENO); // Menutup file descriptor standar output.
    close(STDERR_FILENO); // Menutup file descriptor standar error.

    signal(SIGUSR1, handleSignal); // Mengatur penanganan sinyal untuk SIGUSR1.
    signal(SIGUSR2, handleSignal); // Mengatur penanganan sinyal untuk SIGUSR2.
    signal(SIGRTMIN, handleSignal); // Mengatur penanganan sinyal untuk SIGRTMIN.

    while (1) { // Loop utama daemon.
        DIR *dir; // Struktur untuk direktori.
        struct dirent *ent; // Struktur untuk entri direktori.

        if ((dir = opendir ("sisop_infected")) != NULL) { // Membuka direktori sisop_infected.
            while ((ent = readdir (dir)) != NULL) { // Membaca setiap entri di dalam direktori.
                if (ent->d_type != DT_REG) { // Memeriksa apakah entri adalah file non reguler.
                    checkFile(ent->d_name); // Memeriksa file.
                }
            }
            closedir (dir); // Menutup direktori.
        } else {
            perror ("Could not open directory"); // Menampilkan pesan kesalahan jika tidak dapat membuka direktori.
            exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
        }
        sleep(1); // Menunda eksekusi selama 1 detik.
    }
}

int main(int argc, char *argv[]) { // Fungsi utama.
    if (argc != 2) { // Memeriksa jumlah argumen.
        printf("Usage: %s <security_level>\n", argv[0]); // Menampilkan cara penggunaan program jika argumen tidak sesuai.
        exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
    }

    int level = atoi(argv[1]); // Mengonversi argumen ke integer.
    if (level == 1)
        securityLevel = LOW; // Jika level adalah 1, atur tingkat keamanan ke LOW.
    else if (level == 2)
        securityLevel = MEDIUM; // Jika level adalah 2, atur tingkat keamanan ke MEDIUM.
    else if (level == 3)
        securityLevel = HARD; // Jika level adalah 3, atur tingkat keamanan ke HARD.
    else {
        printf("Invalid security level\n"); // Jika level tidak valid, tampilkan pesan kesalahan.
        exit(EXIT_FAILURE); // Keluar dari program dengan kode kesalahan.
    }

    startDaemon(); // Memulai daemon.

    return 0; // Keluar dari program dengan sukses.
}
```
6. Setelah itu kita bisa langsung melakukan compile dengan `gcc antivirus.c -o antivirus` dan bisa melakukan run dengan `./antivirus 1` untuk level low, `./antivirus 2` untuk level medium, `./antivirus 3` untuk level hard.

7. Berikut ini adalah struktur folder yang saya buat dalam projek soal nomer 4 ini:

![foto_2](/uploads/78559e02cbc768c9b3d9ab6d76338590/foto_2.PNG)

**B. HASIL**
1. Memang benar dalam program yang saya buat, tidak terdapat error yang muncul ketika compile ataupun run. Namun terdapat kekurangan sehingga antivirus tersebut tidak sepenuhnya berjalan dengan baik.

2. Setelah saya melakukan run sesuai 3 level tersebut, saya tidak menerima catatan `virus.log`, tidak mendapati virus-virus dalam folder `sisop_infected` tidak berpindah ke folder `quarantine` maupun terhapus (level hard).

3. Berikut ini adalah dokumentasi dari hal tersebut:

![foto_3](/uploads/a0f266e9c50902def023181794733017/foto_3.PNG)

**C. REVISI**
1. Maka dari itu semua, saya perlu memberbaiki script atau kode program `antivirus.c` sebagai berikut:
```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <dirent.h>
#include <time.h>
#include <pwd.h>
#include <signal.h>

#define FILE_CSV "extensions.csv"
#define LOG_FILE "/home/agas_linux/SISOP_MODUL_2_NOMOR_4_SUSAHBANGETANJIRR/test_7/virus.log"
#define SCAN_FOLDER "sisop_infected"
#define MOVE_FOLDER "/home/agas_linux/SISOP_MODUL_2_NOMOR_4_SUSAHBANGETANJIRR/test_7/quarantine"

volatile sig_atomic_t antivirus_level = 0;

void makeLog(char *owner, char *file, char *action)
{   
    time_t t;
    char timestamp[20];
    struct tm *local_time;
    time(&t);

    local_time = localtime(&t);
    if (local_time != NULL) {
        strftime(timestamp, sizeof(timestamp), "%d-%m-%y:%H-%M-%S", local_time);
    } else {
        printf("localtime failed\n");
        fflush(stdout);
    }
    char message[256];
    snprintf(message, sizeof(message), "[%s][%s] - %s - %s", owner, timestamp, file, action);

    int log = open("/home/agas_linux/SISOP_MODUL_2_NOMOR_4_SUSAHBANGETANJIRR/test_7/virus.log", O_WRONLY | O_APPEND | O_CREAT, 0644);
    if (log == -1) 
    {
        perror("open");
    } 
    else 
    {
        dprintf(log, "%s\n", message);
        close(log);
    }
}

void lowSecurity(char *directory)
{   
    DIR *dir = opendir(directory);
    if (dir == NULL)
    {
        perror("dir: Can't Open sisop_infected Directory");
        exit(EXIT_FAILURE);
    }
    
    struct dirent *entry;
    while((entry = readdir(dir)) != NULL)
    {   
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
        continue;  
    }
        char pathingFile[512];
        snprintf(pathingFile, sizeof(pathingFile), "%s/%s", directory, entry->d_name);

        FILE *csvFile = fopen(FILE_CSV, "r");
        if (csvFile != NULL)
        {
            const char *extension;
            const char *dot = strrchr(entry->d_name, '.');

            if (dot && dot != entry->d_name) {
                extension = dot + 1; 
            } 
            char line[256];
            while(fgets(line, sizeof(line), csvFile))
            {
                if (strstr(line, extension) != NULL)
                {   
                    struct stat fileStat;

                    if (stat(pathingFile, &fileStat) == 0) 
                    {
                    struct passwd *pwd = getpwuid(fileStat.st_uid);
                    if (pwd != NULL) {
                        makeLog(pwd->pw_name, entry->d_name, "Only Warning, Making Log");
                        sleep(1);
                    } else {
                        printf("Owner not found\n");
                        fflush(stdout);
                    }
                    } 
                    else 
                    {
                        perror("stat");
                        exit(EXIT_FAILURE);
                    }
                            
                }
            }
        }
        fclose(csvFile);
    }
    closedir(dir);
}

void moveFile(char *fullPath, char *fileName)
{
    char quarantinePath[256];
    snprintf(quarantinePath, sizeof(quarantinePath), "%s/%s", "/home/agas_linux/SISOP_MODUL_2_NOMOR_4_SUSAHBANGETANJIRR/test_7/quarantine", fileName);
    rename(fullPath, quarantinePath);
}

void mediumSecurity(char *directory)
{   
    DIR *dir = opendir(directory);
    if (dir == NULL)
    {
        perror("dir: Can't Open sisop_infected Directory");
        exit(EXIT_FAILURE);
    }
    
    struct dirent *entry;
    while((entry = readdir(dir)) != NULL)
    {   
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
        continue;  
    }
        char pathingFile[512];
        snprintf(pathingFile, sizeof(pathingFile), "%s/%s", directory, entry->d_name);


        FILE *csvFile = fopen(FILE_CSV, "r");
        if (csvFile != NULL)
        {
            const char *extension;
            const char *dot = strrchr(entry->d_name, '.');

            if (dot && dot != entry->d_name) {
                extension = dot + 1; 
            } 
            char line[256];
            while(fgets(line, sizeof(line), csvFile))
            {
                if (strstr(line, extension) != NULL)
                {   
                    struct stat fileStat;

                    if (stat(pathingFile, &fileStat) == 0) 
                    {
                    struct passwd *pwd = getpwuid(fileStat.st_uid);
                    if (pwd != NULL) {
                        makeLog(pwd->pw_name, entry->d_name, "Moving File to quarantine");
                        moveFile(pathingFile, entry->d_name);
                        sleep(1);
                    } else {
                        printf("Owner not found\n");
                        fflush(stdout);
                    }
                    } 
                    else 
                    {
                        perror("stat");
                        exit(EXIT_FAILURE);
                    }
                            
                }
            }
        }
        fclose(csvFile);
    }
    closedir(dir);
}

void hardSecurity(char *directory)
{   
    DIR *dir = opendir(directory);
    if (dir == NULL)
    {
        perror("dir: Can't Open sisop_infected Directory");
        exit(EXIT_FAILURE);
    }
    
    struct dirent *entry;
    while((entry = readdir(dir)) != NULL)
    {   
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {
        continue;  
    }
        char pathingFile[512];
        snprintf(pathingFile, sizeof(pathingFile), "%s/%s", directory, entry->d_name);

        FILE *csvFile = fopen(FILE_CSV, "r");
        if (csvFile != NULL)
        {
            const char *extension;
            const char *dot = strrchr(entry->d_name, '.');

            if (dot && dot != entry->d_name) {
                extension = dot + 1; 
            } 
            char line[256];
            while(fgets(line, sizeof(line), csvFile))
            {
                if (strstr(line, extension) != NULL)
                {   
                    struct stat fileStat;

                    if (stat(pathingFile, &fileStat) == 0) 
                    {
                    struct passwd *pwd = getpwuid(fileStat.st_uid);
                    if (pwd != NULL) {
                        makeLog(pwd->pw_name, entry->d_name, "File has been Deleted");
                        remove(pathingFile);
                        sleep(1);
                    } else {
                        printf("Owner not found\n");
                        fflush(stdout);
                    }
                    } 
                    else 
                    {
                        perror("stat");
                        exit(EXIT_FAILURE);
                    }
                            
                }
            }
        }
        fclose(csvFile);
    }
    closedir(dir);
}

void downloadCSV(const char *url, const char *fileName)
{
    char *wget_args[] = {
        "wget",
        "--no-check-certificate",
        url,
        "-O",
        fileName,
        NULL
    };

    if (execvp("wget", wget_args) == -1)
    {
        perror("exec : Download Error");
        exit(EXIT_FAILURE);
    }
}

char *rot13(const char *str) {
    char *decoded_line = strdup(str);  
    int i;
    for (i = 0; decoded_line[i]; i++) 
    {
        if (decoded_line[i] == '.') 
        {
            while (decoded_line[i] != '\0') 
            {
                if ((decoded_line[i] >= 'A' && decoded_line[i] <= 'Z')) 
                {
                    decoded_line[i] = (((decoded_line[i] - 'A' + 13) % 26) + 'A');
                } 
                else if ((decoded_line[i] >= 'a' && decoded_line[i] <= 'z')) 
                {
                    decoded_line[i] = (((decoded_line[i] - 'a' + 13) % 26) + 'a');
                }
                i++;
            }
            break;
        }
    }
    return decoded_line;
}

void decodeROT13()
{
    FILE *inputFile, *outputFile;
    char line[256];
    int count = 0;

    inputFile = fopen(FILE_CSV, "r");
    if (inputFile == NULL)
    {
        perror("Can't open extensions.csv");
        return;
    }

    outputFile = fopen("temp.csv", "w");
    if (outputFile == NULL)
    {
        perror("Failed to make Temporary File");
        fclose(inputFile);
        return;
    }

    while (count < 8 && fgets(line, sizeof(line), inputFile) != NULL) {
        fputs(line, outputFile);
        count++;
    }

    while (fgets(line, sizeof(line), inputFile) != NULL) {
        char *decoded_line = rot13(line);
        fputs(decoded_line, outputFile);   
        free(decoded_line);                
    }

    fclose(inputFile);
    fclose(outputFile);

    if (rename("temp.csv", FILE_CSV) != 0) {
        perror("Failed to replace extensions.csv");
    }
}   

void sig_usr(int signo)
{
    if (signo == SIGUSR1)
    {
        antivirus_level = 1;
    }
    else if (signo == SIGUSR2)
    {
        antivirus_level = 2;
    }
    else if (signo == SIGRTMIN)
    {
        antivirus_level = 3;
    }
    else
    {
        perror("Received signal Error");
        exit(EXIT_FAILURE);
    }

}

int main(int argc, const char *argv[])
{   
    if(signal(SIGUSR1, sig_usr) == SIG_ERR)
    {
        perror("Can't catch signal");
    }
    if(signal(SIGUSR2, sig_usr) == SIG_ERR)
    {
        perror("Can't catch signal");
    }
    if(signal(SIGRTMIN, sig_usr) == SIG_ERR)
    {
        perror("Can't catch signal");
    }

    if (argc != 3)
    {
        fprintf(stderr,"Usage: %s -p low/medium/high\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if (strcmp(argv[2], "low") == 0)
    {
        antivirus_level = 1;
    }
    else if (strcmp(argv[2], "medium") == 0)
    {
        antivirus_level = 2;
    }
    else if (strcmp(argv[2], "high") == 0)
    {
        antivirus_level == 3;
    }
    
    /*close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);*/

    pid_t download_id = fork();
    if (download_id == -1)
    {
        perror("fork : Download Error");
        exit(EXIT_FAILURE);
    }

    if (download_id == 0)
    {
        downloadCSV("https://docs.google.com/uc?export=download&id=1gIhwR7JLnH5ZBljmjkECzi8tIlW8On_5", FILE_CSV);
    }
    else
    {
        int downloadStatus;
        wait(&downloadStatus);
            
        int decode_id = fork();
        if (decode_id < 0)
        {
            exit(EXIT_FAILURE);
        }

        if(decode_id == 0)
        {
            decodeROT13();
            exit(EXIT_SUCCESS);
        }
        else
        {
            int decodeStatus;
            wait(&decodeStatus);
            
            pid_t pid, sid;
            pid = fork();
            if (pid < 0)
            {   
                exit(EXIT_FAILURE);
            }

            if (pid > 0)
            {   
                exit(EXIT_SUCCESS);
            }

            sid = setsid();
            if (sid < 0)
            {   
                perror("setid");
                exit(EXIT_FAILURE);
            }

            umask(0);
    
            while(1) 
            {   
                if (antivirus_level == 1)
                {
                    lowSecurity("/home/agas_linux/SISOP_MODUL_2_NOMOR_4_SUSAHBANGETANJIRR/test_7/sisop_infected");
                }
                else if (antivirus_level == 2)
                {
                    mediumSecurity("/home/agas_linux/SISOP_MODUL_2_NOMOR_4_SUSAHBANGETANJIRR/test_7/sisop_infected");
                }
                else if (antivirus_level == 3)
                {
                    hardSecurity("/home/agas_linux/SISOP_MODUL_2_NOMOR_4_SUSAHBANGETANJIRR/test_7/sisop_infected");
                }
            }
        }
    }   
}
```

2. Setelah saya melakukan compile, run, dan sebagainya, inilah yang saya dapatkan:

![revisi_1](/uploads/bec3d7c4a74b8bd01a6cf7e979061933/revisi_1.PNG)


![revisi_2](/uploads/26b95633d48b47e339fee08003e693ea/revisi_2.PNG)


![revisi_3__medium_](/uploads/d109732f33c666ce2993b6b37f89ecac/revisi_3__medium_.PNG)


![revisi_4__hard_](/uploads/4f6434fac90275a8574d34e2d0f00ba2/revisi_4__hard_.PNG)


![revisi_5__hard_](/uploads/79fb5745d5b2e17a89b39a8c8132d6c4/revisi_5__hard_.PNG)


![revisi_SIGUSR](/uploads/f57454305e378a0578fda7f0183a8526/revisi_SIGUSR.PNG)


Dari dokumentasi-dokumentasi tersebut, saya sudah mencoba mulai dari ./antivirus -p low sampai hard dan cukup berhasil untuk membuat file virus.log, Memindahkan ke folder quarantine, dan juga menghapus virus yg ada. Kemudian ketika saya mencoba melakukan `ps aux` saya juga berhasil mengkonfigurasikan  SIGUSR1 untuk mode "low", SIGUSR2 untuk "medium", dan SIGRTMIN untuk mode "hard". 

``

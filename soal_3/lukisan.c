#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <dirent.h>
#include <wait.h>

void create_zip(char path[], char zipName[]){
pid_t id_child = fork();
    if (id_child < 0) {
        perror("Fork failed");
        exit(EXIT_FAILURE);
    } else if (id_child == 0) {
        chdir(path);
        char *args[] = {"zip", "-r", "-m", zipName, zipName, NULL};
        execvp("zip", args);
        perror("Exec failed");
        exit(EXIT_FAILURE);
    }
}

void images_load(char f_path[]){
    pid_t id_child = fork();
    if (id_child < 0){
        exit(EXIT_FAILURE);
    }
    else if (id_child == 0){
        char filename[500], images_url[120];
        time_t t = time(NULL);
        struct tm *current = localtime(&t);
        int imageSize = (int)(t % 1000) + 50;

        sprintf(filename, "%s/%04d-%02d-%02d_%02d:%02d:%02d", f_path, current->tm_year + 1900, current->tm_mon + 1, current->tm_mday, 
        current->tm_hour, current->tm_min, current->tm_sec);  
        sprintf(images_url, "https://source.unsplash.com/random/%dx%d", imageSize, imageSize);

        char *args[] = {"wget", "-q", "-O", filename, images_url, NULL};
        execv("/bin/wget", args);
    }
}

int files_counting(char f_path[]){
    DIR *dirPtr;
    struct dirent *entry;
    int count = 0;
    dirPtr = opendir(f_path);

    if (dirPtr != NULL) {
        while ((entry = readdir (dirPtr))) {
            if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;
            else count++;
        }
        closedir (dirPtr);
    }

    return count;
}

void create_folder(){
    pid_t pid = fork();
    if (pid < 0){
        exit(EXIT_FAILURE);
    }
    else if (pid == 0){
        char foldername[120];
        time_t t = time(NULL);
        struct tm *current = localtime(&t);
        sprintf(foldername, "/home/caca/Downloads/soal_3/%04d-%02d-%02d_%02d:%02d:%02d", current->tm_year + 1900, current->tm_mon + 1, current->tm_mday, 
        current->tm_hour, current->tm_min, current->tm_sec);  
        char *args[] = {"mkdir", foldername, NULL};
        execv("/bin/mkdir", args);
    }
}

void create_killer(pid_t pid, char p){
    char current_dir[100];
    getcwd(current_dir, sizeof(current_dir));
    int status;
    pid_t parent_process = fork();
    if (parent_process < 0)
        exit(EXIT_FAILURE);

    if (parent_process == 0) {
        FILE *filePtr;
        char killer_file[150];
        char code[1000];
        char exec_path[150];
        char file_name[150];
        char run_mode[100];

        sprintf(file_name, "%s/killer.c", current_dir);
        sprintf(exec_path, "%s/killer", current_dir);

        sprintf(killer_file, "%s/killer.c", current_dir);

        if (p == 'a')
          strcpy(run_mode, " char *argv[] = {\"killall\", \"-s\", \"9\", \"lukisan\", NULL}; execv(\"/usr/bin/killall\", argv);");
        else if (p == 'b')
          sprintf(run_mode, " char *argv[] = {\"kill\", \"%d\", NULL}; execv(\"/usr/bin/kill\", argv);", pid);

        sprintf(code,   "#include <stdlib.h>\n"
                        "#include <unistd.h>\n"
                        "#include <stdio.h>\n"
                        "#include <wait.h>\n"
                        "#include <string.h>\n"
                        "#include <sys/types.h>\n"

                        " int main() {"
                        " pid_t id_child;"
                        " int status;"

                        " id_child = fork();"
                        " if (id_child < 0) exit(EXIT_FAILURE);"

                        " if (id_child == 0) {"
                            " %s"
                        " } else {"
                            " while (wait(&status)>0);"
                            " char *argv[] = {\"rm\", \"%s/killer\", NULL};"
                            " execv(\"/bin/rm\", argv);"
                        " }"
                        " }", run_mode, current_dir);

        filePtr = fopen(killer_file, "w");
        fputs(code, filePtr);
        fclose(filePtr);

        char *argv[] = {"gcc", file_name, "-o", exec_path, NULL};
        execv("/usr/bin/gcc", argv);
    }
    while(wait(&status) > 0);
    pid_t remove_killer = fork();

    if (remove_killer < 0) 
      exit(EXIT_FAILURE);

    if (remove_killer == 0) {
        char killer_file_path[150];
        sprintf(killer_file_path, "%s/killer.c", current_dir);
        char *argv[] = {"rm", killer_file_path, NULL};
        execv("/bin/rm", argv);
    }

    while(wait(&status) > 0);
}

int main(int argc, char *argv[]) {
    if(argc != 2 || argv[1][1] != 'a' && argv[1][1] != 'b'){
        printf("Argumen invalid \n");
        return 0;
    }

    pid_t pid1, pid_2;

    pid1 = fork();
    if (pid1 == 0) {

        create_killer(getpid(), argv[1][1]);
        umask(0);

        pid_t sid = setsid();
        if (sid < 0) {
            exit(EXIT_FAILURE);
        }

        if ((chdir("/")) < 0) {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

        while (1) {
            create_folder();
            sleep(30);
        }
    } else if (pid1 < 0) {
        exit(EXIT_FAILURE);
    }

    pid_2 = fork();
    if (pid_2 == 0) {

        umask(0);

        pid_t sid = setsid();
        if (sid < 0) {
            exit(EXIT_FAILURE);
        }

        if ((chdir("/")) < 0) {
            exit(EXIT_FAILURE);
        }

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

        while (1) {
            DIR *dirPtr;
            struct dirent *entry;
            char *path = "/home/caca/Downloads/soal_3";
            dirPtr = opendir(path);
            struct stat sb;

            if (dirPtr != NULL) {
                while ((entry = readdir(dirPtr))) {
                    if (!strcmp(entry->d_name, ".") || !strcmp(entry->d_name, "..")) continue;

                    char f_path[350];
                    sprintf(f_path, "%s/%s", path, entry->d_name);
                    if (stat(f_path, &sb) == -1) continue;

                    if (S_ISDIR(sb.st_mode)) {
                        int count = files_counting(f_path);
                        if (count < 15) images_load(f_path);
                        else create_zip(path, entry->d_name);
                    }
                }
                closedir(dirPtr);
            }
            sleep(5);
        }
    } else if (pid_2 < 0) {
        exit(EXIT_FAILURE);
    }

    return 0;
}


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/wait.h>

int main() {
    char *buatPlayers[] = {"mkdir", "players", NULL};
    pid_t pid = fork();

    if (pid == 0) {
        // Proses Child untuk membuat folder "players"
        execv("/bin/mkdir", buatPlayers);
        exit(1);
    } else {
        int status;
        waitpid(pid, &status, 0);

        // Download file database pemain
        char *donlodFile[] = {"wget", "https://drive.google.com/uc?id=1nwIlFqHjcqsgF0lv0D0MoBN8EHw3ClCK", "-O", "players.zip", NULL};
        pid = fork();

        if (pid == 0) {
            execv("/usr/bin/wget", donlodFile);
            exit(1);
        } else {
            waitpid(pid, &status, 0);

            // Ekstrak "players.zip" ke dalam folder "players"
            char *unzipPlayers[] = {"unzip", "players.zip", "-d", "players", NULL};
            pid = fork();

            if (pid == 0) {
                execv("/usr/bin/unzip", unzipPlayers);
                exit(1);
            } else {
                waitpid(pid, &status, 0);

                // Hapus file "players.zip"
                char *hapusPlayerszip[] = {"rm", "players.zip", NULL};
                pid = fork();

                if (pid == 0) {
                    execv("/bin/rm", hapusPlayerszip);
                    exit(1);
                } else {
                    waitpid(pid, &status, 0);
        
                    DIR *nonCavaliers = opendir("players");
                    struct dirent *entry;

                    if (nonCavaliers != NULL) {
                        while ((entry = readdir(nonCavaliers)) != NULL) {
                            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                                char *namaFoto = entry->d_name;

                                if (strstr(namaFoto, "Cavaliers") == NULL) {
                                    char path[256];
                                    snprintf(path, sizeof(path), "players/%s", namaFoto);
                                    remove(path);
                                }
                            }
                        }
                        closedir(nonCavaliers);
                    }

                    // Kategori pemain dan hitung jumlah pemain untuk masing-masing posisi
                    int pgHitung = 0, sgHitung = 0, sfHitung = 0, pfHitung = 0, cHitung = 0;
                    DIR *mengKategori = opendir("players");
                    if (mengKategori != NULL) {
                        while ((entry = readdir(mengKategori)) != NULL) {
                            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                                char *namaFoto = entry->d_name;
                                char *pos = strtok(namaFoto, "-");

                                if (pos != NULL) {
                                    if (strcmp(pos, "Cavaliers") == 0) {
                                        pos = strtok(NULL, "-");

                                        if (pos != NULL) {
                                            if (strcmp(pos, "PG") == 0) {
                                                pgHitung++;
                                            } else if (strcmp(pos, "SG") == 0) {
                                                sgHitung++;
                                            } else if (strcmp(pos, "SF") == 0) {
                                                sfHitung++;
                                            } else if (strcmp(pos, "PF") == 0) {
                                                pfHitung++;
                                            } else if (strcmp(pos, "C") == 0) {
                                                cHitung++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        closedir(mengKategori);
                    }

                    // Output hasil kategorisasi ke "Formasi.txt"
                    FILE *outputFile = fopen("Formasi.txt", "w");

                    if (outputFile != NULL) {
                        fprintf(outputFile, "PG: %d\nSG: %d\nSF: %d\nPF: %d\nC: %d\n", pgHitung, sgHitung, sfHitung, pfHitung, cHitung);
                        fclose(outputFile);
                    }

                    // Membuat folder "clutch"
                    char *bikinClutch[] = {"mkdir", "clutch", NULL};
                    pid = fork();

                    if (pid == 0) {
                        execv("/bin/mkdir", bikinClutch);
                        
                        exit(1);
                    } else {
                        waitpid(pid, &status, 0);

                        // Copy foto Kyrie Irving ke dalam folder "clutch"
                        char *copyKyrie[] = {"cp", "players/Cavaliers-PG-Kyrie-Irving.png", "clutch/Kyrie.png", NULL};
                        pid = fork();

                        if (pid == 0) {
                            execv("/bin/cp", copyKyrie);
                            exit(1);
                        } else {
                            waitpid(pid, &status, 0);

                            // Copy foto Lebron James ke dalam folder "clutch"
                            char *copyLebron[] = {"cp", "players/Cavaliers-SF-LeBron-James.png", "clutch/Lebron.png", NULL};
                            pid = fork();

                            if (pid == 0) {
                                execv("/bin/cp", copyLebron);
                                exit(1);
                            } else {
                                waitpid(pid, &status, 0);
                            }
                        }
                    }
                }
            }
        }
    }

    return 0;
}
